// Starts the javascript on load
$(function() {
  // Sets active link class based on current url
  // If url is root("/") then hightlight first a href in the menu else hightlight whatever matches
  // the regex
  if(location.pathname === "/") {
    $('nav').children('a:first-child').addClass('active');
  } else {
    $('nav a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
  };

  // Anything with the class help has a popup that will appear on click
  $('.help').popup({ on: 'click' });

  // Get FB App ID
  if(fbAppID) {
    var fbAppID = $("#fb-root")[0].attributes[1].nodeValue;
  };
  // Create an array to store scraper results
  var usersID = [];

  // Stops the Group ID form from submitting
  $('#groupForm').submit(function(e) {
    e.preventDefault();
  });

  // Start Facebook Graph API
  window.fbAsyncInit = function() {
    FB.init({
      appId            : fbAppID,
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.10'
    });
    FB.AppEvents.logPageView();

    // Check if user is logged into Facebook
    FB.getLoginStatus(function(response) {
      if(response.status === 'connected') {
	console.log('Logged in.');
      }
      else {
	console.log('Not logged in');
      }
    });

    $("#groupScrape").click(function() {
      var groupID = $("#groupID").val();
      var url = "/" + groupID + "/members";
      // Calls function scrapeGroup with group ID
      scrapeGroup(url);
    });

    // Gathers the member ID from the group
    function scrapeGroup(url) {
      FB.api(url, function(response) {
	if(response && !response.error) {
	  var i = 0;

	  // Pushes each member id into an array
	  for(i; i < response.data.length; i++) {
	    // $("#scraperResults").append($(response.data[i].id));
	    usersID.push(response.data[i].id);
	  };

	  // Checks if there's a next page and scrapes the next 25 members
	  if(response.paging.next) {
	    scrapeGroup(response.paging.next);
	  } else {
	    console.log("Done");
	    // Convert to string and replaces all "," with a newline
	    usersID = usersID.toString().replace(/,/g, "\n");
	    // Sends the users ID to be created as a textfile
	    createTxtFile(usersID);
	  };
	};
      });
    };

    // Creates a text file with all the usersID that was scraped
    function createTxtFile(usersID) {
      var textFile = null;
      var download = $("#download");

      function makeTextFile(usersID) {
	var data = new Blob([usersID], { type: 'text/plain' });

	// If a file was previously generated, revoke object URL to avoid memory leak
	if(textFile !== null) {
	  window.URL.revokeObjectURL(textFile);
	};

	// Create download URL
	textFile = window.URL.createObjectURL(data);

	// Returns the textfile for download
	return textFile;
      };

      download.click(function() {
	console.log("Getting the download ready..");
	download.attr("href", makeTextFile(usersID));
      });

    };

  };
  // Part of Facebook Graph API
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=161227574466627";
    fjs.parentNode.insertBefore(js, fjs);
  } (document, 'script', 'facebook-jssdk'));

});

