'use strict';

const gulp	  = require('gulp')
      , inject	  = require('gulp-inject');

gulp.task('inject', () => {
  let target  = gulp.src([
    'public/partials/header.ejs',
    'public/partials/footer.ejs'
  ]),
      sources = gulp.src([
	'public/css/*.css',
	'public/js/*.js'
      ], { 
	read: false 
      });

  return target
    .pipe(inject(sources, {
      ignorePath: 'public',
      addPrefix: '/static'
    }))
    .pipe(gulp.dest('public/partials/'));
});

gulp.task('default', [
  'inject'
]);
