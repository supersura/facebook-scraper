# Facebook Scraper
Created to get the user ID from groups, pages, apps, and posts.

You can use the user IDs to create more targeted ads to get a better ROI.

Created by supersura.
