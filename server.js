'use strict';

const config		    = require('./config/config')
      , fb		    = require('./config/facebook')
      , express		    = config.express
      , app		    = express()
      , scraperRoutes	    = require('./scraper/routes/scraperRoutes');

// Creating new prefix for static files
app.use('/static', express.static('./public'));

// Set view folder for the app
app.set('views', config.path.join(__dirname + '/public/views'));

// Set view engine for the app
app.set('view engine', 'ejs');

// Set Facebook App ID for the app
app.fbAppID = fb.appID;

// Route for homepage
app.get('/', (req, res) => {
  res.render('homepage.ejs');
});

// Routes for the scraper
app.use('/scraper', scraperRoutes);

// Runs the server
app.listen(3000, () => {
  console.log("The app is running on port: 3000");
});
