'use strict';

const config		    = require('../../config/config')
      , scraperRoutes	    = config.express.Router()
      , scraperController   = require('../controllers/scraperController');

// When '/scraper' is hit, renders the index.ejs file
scraperRoutes.get('/', scraperController.index);

// Export the const scraperRoutes to allow other parts of to call its routes
module.exports = scraperRoutes;
