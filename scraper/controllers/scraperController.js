'use strict';

const config		    = require('../../config/config')
      , scraperController   = {
	// When user is his '/scraper', render this page
	index: (req, res) => {
	  res.render('scraper/index.ejs', {
	    fbAppID: req.app.fbAppID
	  });
	},
      }

// Export the const scraperController to allow other parts of the app to call it
module.exports = scraperController;
