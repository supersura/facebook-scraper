'use strict';

const facebook = {
  appID: process.env.FB_APP_ID 
};

module.exports = facebook;
