'use strict';

const config = {
  express	: require('express')
  , path	: require('path')
  , bodyParser	: require('body-parser')
};

// Export the const config to allow other parts of the app to call it
module.exports = config;
